# 1. CLI Tool

> You need to implement a CLI tool that takes a list of URLs as input, visits each URL and prints to the output the list 
> of pairs: URL and response body size. The output must be sorted by the size of the response body. 

## How to Launch

    cat tests/urls.txt | go run ./cmd/url-stat/main.go

# 2. k8s app architecture

> You need to design the architecture of the system that will run in Kubernetes. You can use any desired format (text, UML diagrams, k8s manifests, hand drawings, etc.) to share the result.
The system has the following components:
> - SPA frontend
> - API backend
> - Postgres cluster
> - S3 bucket
> - external data provider (JSON, HTTP/1.1)
> - SQL script for initializing the database
> - a binary that creates fixtures

## Solution

Minimal required components for launching the app in accordance with the conditions.

![k8s app architecture](./img/app_architecture.jpg)

### Notable Points

1. We are building SPA as static bundles and uploading its on CDN to minimize latency for the user.
2. We are using standard k8s deployment for launching API backend.
3. We are using ConfigMap/Secrets for passing required configuration values to the app (like S3 secrets).
4. We are using caching proxy in front of External Data Provided to reduce requests to the service (in order not to exceed the possible request limits).
5. We are using PostgreSQL deployed in cluster mode based on HA operator solution (like zalando or percona) with some proxy (ProxySQL or HA Proxy) in front.
6. We have additional `Fixture Job` which can be used to apply fixtures to testing environments.
7. In addition, we can use HPA for automatically scale the app based on gathered matrices.
