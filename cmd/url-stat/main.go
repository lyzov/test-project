package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	ContentLengthHeader = "Content-Length"
)

type UrlInfo struct {
	Url  string
	Size int
}

type FetchResult struct {
	Info *UrlInfo
	Err  error
}

type FetchResults chan *FetchResult

func NewFetchError(err error) *FetchResult {
	return &FetchResult{Err: err}
}

func NewFetchResult(url string, size int) *FetchResult {
	return &FetchResult{
		Info: &UrlInfo{
			Url:  url,
			Size: size,
		},
	}
}

func fetchBodySize(url string) (int, error) {
	resp, err := http.Head(url)
	if err != nil {
		return 0, err
	}
	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("server returns %v status for url %v", resp.StatusCode, url)
	}
	return strconv.Atoi(resp.Header.Get(ContentLengthHeader))
}

func fetchInfo(url string, results FetchResults) {
	size, err := fetchBodySize(url)
	if err != nil {
		results <- NewFetchError(err)
		return
	}
	results <- NewFetchResult(url, size)
}

func main() {
	uniqueUrls := make(map[string]struct{})

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		url := strings.TrimSpace(scanner.Text())
		if url == "" {
			continue
		}
		if _, ok := uniqueUrls[url]; !ok {
			uniqueUrls[url] = struct{}{}
		}
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	urlsCount := len(uniqueUrls)
	fetchResults := make(FetchResults, urlsCount)
	defer close(fetchResults)
	for url := range uniqueUrls {
		go fetchInfo(url, fetchResults)
	}

	var result []*UrlInfo
	var errs []error
	for i := 0; i < urlsCount; i++ {
		r := <-fetchResults
		if r.Err != nil {
			errs = append(errs, r.Err)
		} else {
			result = append(result, r.Info)
		}
	}

	if len(errs) > 0 {
		fmt.Println("Unable to fetch body size for some of urls:")
		for _, err := range errs {
			fmt.Println(err)
		}
		os.Exit(1)
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i].Size < result[j].Size
	})

	for _, info := range result {
		fmt.Printf("%d\t%s\n", info.Size, info.Url)
	}
}
